export class Helper {
    static randomInRange (max = 10000000, min = 1000) {
        return Math.floor(Math.random() * (min - 1000 + 1)) + min;
    }
    static getFormData (selector = '.createWrapper') {
        let data = {};
        for(var pair of new FormData(document.querySelector(selector))) {
            data[pair[0]] = pair[1];
        }

        return data;
    }
    static getYearForView (value) {
        let year = value;
        if ((value + '').length === 4) {
            year = value + '-01-01';
        }

        return year;
    }
}