export class BookForm {
    constructor() {
        this.REGS = {
            text: /\S+/,
            number: /^[+-]?\d+(\.\d+)?$/
        },
        this.form = {
            title: {
                type: 'text',
                required: true
            },
            author: {
                type: 'text',
                required: true
            },
            year: {
                type: 'Date'
            },
            genre: {
                type: 'text'
            },
            pages: {
                type: 'number'
            },
            price: {
                type: 'number',
                step: 0.1,
                required: true
            },
            amount: {
                type: 'number',
                required: true
            },
            publisher: {
                type: 'text',
                required: true
            }
        }
    }
    getForm () {
        return this.form;
    }
    validateBook (data) {
        let validationErrors = [];
        for (let [key, value] of Object.entries(data)) {
            if (!this.validateInput(this.form, key, value)) {
                validationErrors.push(key);
            }
        }

        return validationErrors;
    }
    validateInput (form, name, value = '') {
        let res = false, input = form[name];
        if (value && input) {
            if (input.required) {
                if (this.REGS[input.type].test(value)) {
                    res = true;
                }
            } else {
                res = true;
            }
        }
        if (!value && input && !input.required) {
            res = true;
        }
        return res;
    }
}
