const URLS = {
    books: '../books.json',
    publishers: '../publishing_companies.json',
    genres: '../genres.json',
    addBook: 'https://jsonplaceholder.typicode.com/posts',
    editBook: 'https://jsonplaceholder.typicode.com/posts/1',
    deleteBook: 'https://jsonplaceholder.typicode.com/posts/1'
};

export class Api {
    static request (url, type = 'GET', data = null) {
        return new Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open(type, url, true);

            xhr.onload = function() {
                if (this.status == 200 || (type === 'POST' && this.status == 201)) {
                    resolve(JSON.parse(this.response));
                } else {
                    var error = new Error(this.statusText);
                    error.code = this.status;
                    reject(error);
                }
            };

            xhr.onerror = function() {
                reject(new Error('Network Error'));
            };
            if (data) {
                xhr.send(data);
            } else {
                xhr.send();
            }
        });
    }
    static async makeRequest (link, type = 'GET', data = null) {
        if (URLS[link]) {
            return await this.request(URLS[link], type, data);;
        } else {
            return [];
        }
    }
}