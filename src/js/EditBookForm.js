import { BookForm } from './BookForm';
export class EditBookForm extends BookForm{
    constructor() {
        super();
        this.form.id = {
            type: 'hidden'
        };
        ['amount', 'publisher'].map(field => {
            this.form[field].forbidEdit = true;
            this.form[field].required = false;
        });
    }
}