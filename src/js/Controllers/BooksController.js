import '../../css/main.scss';
import 'babel-polyfill';
import { Handler } from '../Handler';
import { Helper } from '../Helper';
import { BooksModel } from '../Models/BooksModel';
import { AddBookForm } from '../AddBookForm';
import { EditBookForm } from '../EditBookForm';
import { BooksView } from '../Views/BooksView';
import { BooksStorage } from '../BooksStorage';
import { Dropdown } from '../Dropdown';
const AddBookFormObj = new AddBookForm();
const EditBookFormObj = new EditBookForm();

export class BooksController {
    constructor () {
        this.init()
    }
    init () {
        BooksView.renderList(AddBookFormObj.getForm());
        BooksView.renderList(EditBookFormObj.getForm(), '#editModal .modal-body', 'EditInput');
        BooksModel.getBooks().then(response => {
            BooksStorage.setBooks(response.books);
            BooksStorage.setPublishers(response.publishers);
            BooksStorage.setGenres(response.genres);
            new Dropdown(response.publishers, 'publisherAddBookForm');
            new Dropdown(response.genres, 'genreAddBookForm');
            new Dropdown(response.genres, 'genreEditInput');

            BooksView.buildBooks(response.books);
        }).catch(error => console.log(error));
        Handler.add(this);


    }
    searchInputChanged (searchVal) {
        BooksView.buildBooks(BooksStorage.getBooks(), searchVal);
    }
    addBookBtnClicked () {
        let data = Helper.getFormData(),
            validationErrors = AddBookFormObj.validateBook(data);
        if (validationErrors.length) {
            BooksView.addErrors(validationErrors);
        } else {
            BooksModel.addBook(data).then(() => {
                data.id = Helper.randomInRange();
                BooksStorage.addBook(data);
                BooksView.createRow(data);
                BooksView.clearInputs();
            }).catch(error => console.log(error));
        }
    }
    editBookBtnClicked (id) {
        BooksView.fillInputs(BooksStorage.getBookById(id))
        BooksView.showModal();
    }
    saveBookBtnClicked (id) {
        let data = Helper.getFormData('#editModal form'),
            validationErrors = EditBookFormObj.validateBook(data),
            oldBook = BooksStorage.getBookById(id);
        data.publisher = oldBook.publisher;
        data.amount = oldBook.amount;
        if (validationErrors.length) {
            BooksView.addErrors(validationErrors, '#editModal');
        } else {
            BooksModel.editBook(data).then(() => {
                BooksStorage.editBook(data);
                BooksView.changeRow(data);
                BooksView.hideModal();
            }).catch(error => console.log(error));
        }
    }
    deleteBookClicked (id) {
        BooksModel.deleteBook(id).then(() => {
            BooksStorage.deleteBook(id);
            BooksView.removeRow(id);
        }).catch(error => console.log(error));
    }
}
