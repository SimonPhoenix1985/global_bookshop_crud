import { Api } from '../Api';

export class BooksModel {

    static getBooks () {
        return Api.makeRequest('books');
    }

    static addBook (data) {
        return Api.makeRequest('addBook', 'POST', JSON.stringify(data));
    }

    static editBook (data) {
        return Api.makeRequest('editBook', 'PUT', JSON.stringify(data));
    }

    static deleteBook (id) {
        return Api.makeRequest('deleteBook', 'DELETE', id);
    }
}