import { Helper } from '../Helper';
export class BooksView {
    static buildBooks (books, search = '') {
        document.getElementById('booksTable').innerHTML = '';
        books.map((book) => {
            if (search) {
                if (Object.values(book).join('').includes(search)) {
                    this.createRow(book);
                }
            } else {
                this.createRow(book);
            }
        });
    }
    static changeRow (book) {
        Array.from(document.querySelector(`tr[data-id="${book.id}"]`).getElementsByTagName('td')).map(td => {
            if (td.dataset.name) {
                td.innerHTML = book[td.dataset.name];
            }
        });
    }
    static createRow (book) {
        let tableRef = document.getElementById('booksTable');
        let row = tableRef.insertRow(0);
        row.setAttribute('data-id', book.id);
        ['title', 'author', 'publisher', 'amount', 'price'].map(key => {
            row.innerHTML += `<td data-name="${key}">${book[key]}</td>`
        });
        
        row.innerHTML += `
            <td>
                <button data-id="${book.id}" class="editBtn">Edit</button>
                <button data-id="${book.id}" class="deleteBtn">Delete</button>
            </td>
        `;
    }
    static removeRow (id) {
        document.querySelector(`tr[data-id="${id}"]`).remove();
    }
    static renderList (form, selector = '.createWrapper', type) {
        Object.keys(form).map( input => document.querySelector(selector).innerHTML += this.getInputHtml(input, form, type));
    }
    static getInputHtml (input, form, type = 'AddBookForm') {
        return `
            <p class="${form[input].type === 'hidden' ? 'hidden' : ''}">
            <label for="${input}${type}">${input} ${form[input].required ? '<span class="required">*<span>' : ''}</label>
            <input type="${form[input].type}"
             id="${input}${type}"
             name="${input}"
             ${form[input].forbidEdit ? 'disabled' : ''}
            ${form[input].step ? 'step=' + form[input].step : ''}>
        </p>
        `;
    }

    static clearInputs (selector = '.createWrapper') {
        Array.from(document.querySelector(selector)).map(input => {
            input.value = '';
            input.classList.remove('failedValidation');
        });
    }
    static fillInputs (book) {
        for (let [key, value] of Object.entries(book)) {
            if (key === 'year') {
                value = Helper.getYearForView(value);
            }
            if (key === 'id') {
                document.querySelector(`#editModal .save`).setAttribute('data-id', value);
            }
            document.querySelector(`#editModal #${key}EditInput`).value = value;
        }
    }
    static addErrors (validationErrors, selector = '.createWrapper') {
        Array.from(document.querySelectorAll(`${selector} input`)).map(el => {
            el.classList.remove('failedValidation');
        });
        validationErrors.map(input => {
            document.querySelector(`${selector} input[name="${input}"]`).classList.add('failedValidation');
        });
    }

    static showModal (el = 'overlay') {
        document.getElementsByClassName(el)[0].style.display = 'block';
    }

    static hideModal (el = 'overlay') {
        document.getElementsByClassName(el)[0].style.display = 'none';
    }
}