const EDIT_MODAL = document.getElementById('editModal');
import {BooksView} from './Views/BooksView';
export class Handler {
    static add (app) {
        document.getElementById('booksTable').addEventListener('click', function (el) {
            let method = 'editBookBtnClicked';
            switch (el.target.className) {
                case 'deleteBtn': {
                    method = 'deleteBookClicked';
                    break;
                }
            }
            if (el.target.dataset.id) {
                app[method](el.target.dataset.id);
            }
        }, false);

        document.querySelector('.tableWrapper .searchInput').oninput = el => app.searchInputChanged(el.target.value);

        document.getElementsByClassName('addBookBtn')[0].addEventListener('click', app.addBookBtnClicked);

        Array.from(EDIT_MODAL.getElementsByClassName('close')).map(el => el.onclick = () => BooksView.hideModal());

        document.querySelector('#editModal .save').addEventListener('click', el => {
            if (el.target.dataset.id) {
                app.saveBookBtnClicked(el.target.dataset.id);
            }
        });
    }
}