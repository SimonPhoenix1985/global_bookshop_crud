let _books = new WeakMap();
let _publishers = new WeakMap();
let _genres = new WeakMap();

export class BooksStorage {
    static setBooks (books) {
        _books.set(this, books);
    }
    static getBooks () {
        return _books.get(this);
    }
    static getBookById (id) {
        let books = this.getBooks(), res;
        books.some((book) => {
            if (book.id == id) {
                res = book;
            }
        });
        return res;
    }
    static addBook (book) {
        let books = this.getBooks();
        books.unshift(book);
        this.setBooks(books);
    }
    static editBook (newBook) {
        let books = this.getBooks();
        books.some((book) => {
            if (book.id == newBook.id) {
                book = newBook;
            }
        });
        this.setBooks(books);
    }
    static deleteBook (id) {
        let books = this.getBooks();
        books.some((book, index) => {
            if (book.id == id) {
                books.splice(index, 1);
            }
        });
        this.setBooks(books);
    }
    static setPublishers (publishers) {
        _publishers.set(this, publishers);
    }
    static getPublishers () {
        return _publishers.get(this);
    }
    static setGenres (genres) {
        _genres.set(this, genres);
    }
    static getGenres () {
        return _genres.get(this);
    }
}