export class Dropdown {
    constructor (list, id, selected = 0) {
        this.list = list;
        this.id = id;
        this.dropdownId = this.id + 'Dropdown';
        this.selected = this.list[selected];
        this.setInputValue(this.selected);
        this.build();
    }
    build () {
        let el = document.getElementById(this.id);
        let dropdown = document.createElement('div');

        dropdown.classList.add('dropdown');
        dropdown.setAttribute('id', this.dropdownId);
        dropdown.innerHTML = `
            <div class='dropdown-button'>${this.selected}</div>
            <span class='triangle'>&#9660;</span>
            <div class="dropdown-content">
                <input type="text" placeholder="Search.." class="dropdownSearchInput">
                <ul>
                    ${this.list.map(li => `<li class="li" data-text="${li}">${li}</li>`).join('')}
                </ul>
            </div>    
        `;
        el.parentNode.insertBefore(dropdown, el.nextSibling);
        el.style.display = 'none';
        document.querySelector(`#${this.dropdownId} .dropdown-button`).onclick = this.toggle.bind(this);
        document.querySelector(`#${this.dropdownId} .dropdownSearchInput`).oninput = this.filter.bind(this);
        document.getElementById(`${this.dropdownId}`).onclick = event => {
            if (event.target.dataset.text) {
                document.querySelector(`#${this.dropdownId} .dropdown-button`).innerHTML = event.target.dataset.text;
                this.selected = event.target.dataset.text;
                this.setInputValue();
                this.toggle();
            }
        }
    }
    filter (el) {
        let filter, li, i, div;
        filter = el.target.value.toUpperCase();
        div = document.getElementById(this.dropdownId);
        li = div.getElementsByTagName('li');
        for (i = 0; i < li.length; i++) {
            if (li[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
    setInputValue () {
        document.getElementById(this.id).value = this.selected;
    }
    toggle () {
        document.querySelector(`#${this.dropdownId} .dropdown-content`).classList.toggle('show');
    }
}